# Package Bilan

Cette librairie vise à extraire tout un tas d'indicateurs à partir des postes
du bilan d'une entreprise. Ces indicateurs sont calculés en fonction du type de
bilan :
- Liasse 2033 : bilan simplifié [plus d'infos ici](https://www.impots.gouv.fr/portail/formulaire/2050-liasse/liasse-fiscale-du-regime-reel-normal-en-matiere-de-bic-et-dis)
- Liasse 2050 : bilan complet [plus d'infos ici](https://www.impots.gouv.fr/portail/formulaire/2033-sd/bilan-simplifie)

Elle permet également de calculer des ratios à partir des éléments comptables
pré-calculés (en cours).

#### Architecture du projet

1. Le dossier [data](data/) contient 2 fichiers :
  - [columns.csv](data/columns.csv) : fichier contenant pour chaque ligne le nom
  des champ de chaque poste du bilan ainsi que sa signification et sa source
  (liasse 2033 ou liasse 2050).
  - [features.xls](data/features.xls) : fichier excel détaillant l'ensemble des
  indicateurs calculés avec la définition associée.
  - [dico.json](data/dico.json) : dictionnaire de mapping entre les éléments
  comptables à calculer, les ratios et les fonctions implémentant les formules.
  La liste des indicateurs calculés est détaillée dans un fichier
  de définition (à venir).

2. Le dossier [example](example/) contient un extrait de données au format `.csv`
contenant un sous-ensemble de postes brutes du bilan permettant de calculer
différents indicateurs. Exemple de calcul pour le chiffre d'affaire :
  - `FL` : formule du CA pour la liasse 2050
  - `_210` + `_214` + `_218` : formule du CA pour liasse 2033

#### Installation du package

Télécharger le projet et aller à la racine du répertoire :
```
cd path/to/project/bilan
```

Installation du package :
```
python setup.py install
```

Voir [cet exemple](example/simple_example.py) pour prendre en main la librairie.
