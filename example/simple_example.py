import pandas as pd
from bilan import BilanExtractor

# Charger un fichier tabulaire de bilan
df = pd.read_csv('sample_bilan.csv')

# Calculer les éléments comptable
bilan = BilanExtractor(indicators_list='all',
                       ignore_indicators=['bilan_resultat_net',
                                          'bilan_tresorerie'],
                       compute_ratios=True,
                       verbose=2)
new_df = bilan.fit_transform(X=df)
print(new_df)
